//
//  LocalManager.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 05/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation

class LocalManager {
    
    // MARK: data constant names
    private let userLogged = "user"
    private let favorites = "favorites"

    func getUser() -> User? {
        if let data = UserDefaults.standard.object(forKey: userLogged) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: data) as? User {
                return user
            } else {return nil}
        } else {return nil}
    }
    
    func setUser(user: User) {
        let data = NSKeyedArchiver.archivedData(withRootObject: user)
        UserDefaults.standard.set(data, forKey: userLogged)
    }
    
    func getFavorites() -> [Media]? {
        if let data = UserDefaults.standard.object(forKey: favorites) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Media] {
                return user
            } else {return nil}
        } else {return nil}
    }
    
    func setFavorites(media: [Media]) {
        let data = NSKeyedArchiver.archivedData(withRootObject: media)
        UserDefaults.standard.set(data, forKey: favorites)
    }
    
}
