//
//  SingletonManager.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation

class SingletonManager {

    private let userSave = "user"
    // MARK: Shared Instance
    static let sharedInstance: SingletonManager = {
        let instance = SingletonManager()
        return instance
    }()

    var user: User?

    func getUser() -> User? {
        return LocalManager().getUser()
    }

    func setUser(value: User) {
        LocalManager().setUser(user: value)
        user = value

    }

    func getFavorites() -> [Media]? {
        return LocalManager().getFavorites()
    }

    func setFavorites(value: Media) {
        var favorites = getFavorites()
        if favorites != nil {
            var isFavorite = false
            for favorito in favorites! {
                if favorito.id == value.id {
                    isFavorite = true
                    break
                }
            }
            if !isFavorite {
                favorites?.append(value)
            }

        } else {
            favorites = []
            favorites?.append(value)
        }
        LocalManager().setFavorites(media: favorites!)
    }
}
