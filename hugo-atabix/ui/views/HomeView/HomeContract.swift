//
//  HomeContract.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation
protocol HomeView: BaseView {
    func showTimeLine(media: [Media])
    func goToProfile(user: User)
}

protocol HomeActionListener: BaseActionListener {
 
    func likePost(media: Media)
    func goToProfile(idUser: String)
}
