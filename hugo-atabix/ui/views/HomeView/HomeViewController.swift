//
//  HomeViewController.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation
import UIKit
import Reachability

class HomeViewController: BaseViewController, HomeView {

    /* Outlets */
    // MARK: Section - Outlets

    @IBOutlet weak var tableView: UITableView!

    /* Public Vars */
    // MARK: Section - Public Vars

    var presenter: HomeActionListener?

    /* Private Vars */
    // MARK: Section - Private Vars

    var arrayMedia: [Media] = []
    let user = SingletonManager.sharedInstance.getUser()
    /* UIViewController */
    // MARK: Section - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        completeView()
        presenter?.viewDidLoad()
        let reachability = Reachability()
        if reachability?.connection == Reachability.Connection.none {
            let alertView = UIAlertController(title: "Error",
                                              message: "Network Connection Lost", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "OK", style: .default)
            { action in
                self.wireframe?.presentFavortiesScreenFrom(viewController: self)
            })
            self.present(alertView, animated: true, completion: nil )
        }
    }

    // MARK: Section - Protocol Login View
    func showTimeLine(media: [Media]) {
      
        arrayMedia = media
        tableView.reloadData()
    }

    func goToProfile(user: User) {
        wireframe?.presentProfileScreenFrom(viewController: self, user: user)
    }

    /* Public Methods */
    // MARK: Section - Public Methods

    /* Actions */
    // MARK: Section - Actions

    /* Delegates */
    // MARK: Section - Delegates

    /* Private Methods */
    // MARK: Section - Private Methods
    func completeView() {
        self.navigationItem.title = "Timeline"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.dataSource = self
        tableView.delegate = self
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:
            UIImage.init(named: "favoritoCard"), style: .plain, target: self, action: #selector(favorties))

    }

    @objc func favorties() {
        wireframe?.presentFavortiesScreenFrom(viewController: self)
    }

}
extension HomeViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500.0
    }
}

// MARK: - UITableViewDataSource
extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMedia.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimeLineCell",
                                                 for: indexPath) as! HomeTableViewCell
        cell.setInfoCell(media: arrayMedia[indexPath.row], user: user!)
        cell.delegate = self
        return cell
    }
}

extension HomeViewController: HomeTableViewCellDelegate {
    func goToProfile(idUser: String) {
        presenter?.goToProfile(idUser: idUser)
    }

    func likePost(media: Media) {
        presenter?.likePost(media: media)
    }

}
