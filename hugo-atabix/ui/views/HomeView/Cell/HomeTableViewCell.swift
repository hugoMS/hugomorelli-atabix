//
//  HomeTableViewCell.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

protocol HomeTableViewCellDelegate: class {

    func likePost(media: Media)
    func goToProfile(idUser: String)

}

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak private var itemFeedImageView: UIImageView?
    @IBOutlet weak private var itemFeedUserImageView: UIImageView?
    @IBOutlet weak var itemFeedDate: UILabel!
    @IBOutlet weak private var itemFeedUsernameLabel: UILabel?
    @IBOutlet weak var likeButton: UIButton!

    weak var delegate: HomeTableViewCellDelegate?
    var checked = false
    var idPost: Media?
    var idUser: String?
    public func setInfoCell(media: Media, user: User) {
        self.idPost = media
        self.idUser = user.id
        self.itemFeedUserImageView?.contentMode = UIViewContentMode.scaleAspectFill
        self.itemFeedUserImageView?.clipsToBounds = true
        self.itemFeedUserImageView?.layer.cornerRadius = (self.itemFeedUserImageView?.frame.size.height)! / 2.0
        itemFeedUserImageView?.kf.setImage(with: user.profilePicture)
        self.itemFeedDate.text = media.createdDate?.ddMMMyyyyFormat()
        self.itemFeedUsernameLabel?.text = media.user?.fullName

        self.itemFeedImageView?.contentMode = UIViewContentMode.scaleAspectFill
        self.itemFeedImageView?.clipsToBounds = true
        itemFeedImageView?.kf.setImage(with: media.images?.standardResolution?.url)
    }

    @IBAction func likePost(_ sender: UIButton) {
        if !checked {
            delegate?.likePost(media: idPost!)
            sender.setImage(UIImage(named: "favoritoCard"), for: .normal)
            checked = true
        }
    }

    @IBAction func goToProfile(_ sender: UIButton) {
        delegate?.goToProfile(idUser: idUser!)
    }

}
