//
//  HomePresenter.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation

class HomePresenter: NSObject, HomeActionListener {

    /* Private Vars */
    // MARK: Section - Private Vars
    private var view: HomeView?
    private var instaIteractor: InstagramIteractor?

    /* Constructor */
    // MARK: Section - Constructor

    init (_ view: HomeView, iteractor: InstagramIteractor) {
        self.view = view
        self.instaIteractor = iteractor

    }

    /* Protocol Splash ActionListener */
    // MARK: Section - Protocol Splash ActionListener

    func viewDidLoad() {
        instaIteractor?.getTimeLine(completion: { (media) in
            if let media = media {
                self.view?.showTimeLine(media: media)

            }
        })
    }
    
    func likePost(media: Media) {
        instaIteractor?.likePost(media: media)
    }
    
    func goToProfile(idUser: String) {
        
        instaIteractor?.getUser(idUser: idUser, completion: { (user) in
            if user != nil {
                self.view?.goToProfile( user: user!)
                
            }
        })
        
    }
    /* Delegates */
    // MARK: Section - Delegates

    /* Private Methods */
    // MARK: Section - Private Methods

}
