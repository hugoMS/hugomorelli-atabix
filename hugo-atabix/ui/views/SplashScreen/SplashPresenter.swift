//
//  SplashPresenter.swift
//  hugo-atabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation

class SplashPresenter: NSObject, SplashActionListener {

    /* Private Vars */
    // MARK: Section - Private Vars
    private var view: SplashView?
    private var instaIteractor: InstagramIteractor!

    /* Constructor */
    // MARK: Section - Constructor

    init (_ view: SplashView, iteractor: InstagramIteractor) {
        self.view = view
        self.instaIteractor = iteractor

    }

    /* Protocol Splash ActionListener */
    // MARK: Section - Protocol Splash ActionListener

    func viewDidLoad() {

        if instaIteractor.isLogin() {
            view?.goToHome()
            
        } else {
            view?.goToLogin()
        }

    }
    /* Delegates */
    // MARK: Section - Delegates

    /* Private Methods */
    // MARK: Section - Private Methods

}
