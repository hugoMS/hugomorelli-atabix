//
//  TabsViewController.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import UIKit

class TabsViewController: UITabBarController {
    
    /* Outlets */
    // MARK: Section - Outlets
    
    /* Public Vars */
    // MARK: Section - Public Vars
    
    /* Private Vars */
    // MARK: Section - Private Vars
    
    /* UIViewController */
    // MARK: Section - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // En este viewcontroller se hara toda la configuracion que requieran los tabs por diseno.
        
        // Do any additional setup after loading the view.
        configureTabBarItems()
        configureBarUI()
    }
    
    /* Protocol Base View */
    // MARK: Section - Protocol Base View
    
    /* Public Methods */
    // MARK: Section - Public Methods
    
    /* Actions */
    // MARK: Section - Actions
    
    /* Delegates */
    // MARK: Section - Delegates
    
    /* Private Methods */
    // MARK: Section - Private Methods
    
    private func configureTabBarItems() {
        
        self.viewControllers?[0].tabBarItem =
            UITabBarItem(title: nil,
                         image: UIImage(named: "escenasOff")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal),
                         selectedImage: UIImage(named: "escenasOn")?
                            .withRenderingMode(UIImageRenderingMode.alwaysOriginal))
        
        self.viewControllers?[1].tabBarItem =
            UITabBarItem(title: nil,
                         image: UIImage(named: "profileOff")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal),
                         selectedImage: UIImage(named: "profileOn")?
                            .withRenderingMode(UIImageRenderingMode.alwaysOriginal))
//        
//        self.viewControllers?[2].tabBarItem =
//            UITabBarItem(title: nil,
//                         image: UIImage(named: "profileOff")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal),
//                         selectedImage: UIImage(named: "profileOn")?
//                            .withRenderingMode(UIImageRenderingMode.alwaysOriginal))
//        
        for tabBarItem in self.tabBar.items! {
            tabBarItem.title = ""
            tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        }
        
    }
    
    private func configureBarUI() {
        self.tabBar.isTranslucent = false
        self.tabBar.selectionIndicatorImage = self.backgroundImageForSelectedTab()
    }
    
    private func backgroundImageForSelectedTab() -> UIImage {
        
        let totalTabs  = Float((self.viewControllers?.count)!)
        let totalWidth = UIScreen.main.bounds.size.width
        let tabHeight  = self.tabBar.frame.size.height
        
        let size       = CGSize(width: (totalWidth / CGFloat(totalTabs)), height: tabHeight)
        
        let backColor  = UIColor.clear
        let lineColor  = UIColor(red: 24.0 / 255.0, green: 179.0 / 255.0, blue: 253.0 / 255.0, alpha: 1.0)
        
        let lineHeight = CGFloat(3)
        let margin     = CGFloat(5)
        
        let rectComplete = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        let rectLine = CGRect(x: margin, y: (size.height - lineHeight),
                              width: (size.width - (margin * 2)), height: lineHeight)
        
        var image: UIImage?
        UIGraphicsBeginImageContext(rectComplete.size)
        
        if let ctx = UIGraphicsGetCurrentContext() {
            
            ctx.setFillColor(backColor.cgColor)
            ctx.fill(rectComplete)
            
            ctx.setFillColor(lineColor.cgColor)
            ctx.fill(rectLine)
            
            image = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()
        }
        
        return image!
    }
    
}
