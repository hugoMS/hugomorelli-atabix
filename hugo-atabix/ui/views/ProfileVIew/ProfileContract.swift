//
//  ProfileContract.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 05/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation

protocol ProfileView: BaseView {
    
    func showProfile( user: User)
}

protocol ProfileActionListener: BaseActionListener {
    
}
