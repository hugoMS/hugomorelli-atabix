//
//  ProfilePresenter.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 05/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation

class ProfilePresenter: NSObject, ProfileActionListener {
    
    /* Private Vars */
    // MARK: Section - Private Vars
    private var view: ProfileView?
    private var instaIteractor: InstagramIteractor?
    private var user: User?
    
    /* Constructor */
    // MARK: Section - Constructor
    
    init (_ view: ProfileView, iteractor: InstagramIteractor, user: User) {
        self.view = view
        self.user = user
        self.instaIteractor = iteractor
        
    }
    
    /* Protocol Splash ActionListener */
    // MARK: Section - Protocol Splash ActionListener
    
    func viewDidLoad() {
        self.view?.showProfile(user: user!)

    }
    
    /* Delegates */
    // MARK: Section - Delegates
    
    /* Private Methods */
    // MARK: Section - Private Methods
    
}
