//
//  ProfileViewController.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 05/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class ProfileViewController: BaseViewController, ProfileView {

    /* Outlets */
    // MARK: Section - Outlets
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    /* Public Vars */
    // MARK: Section - Public Vars

    var presenter: ProfileActionListener?

    /* Private Vars */
    // MARK: Section - Private Vars

    /* UIViewController */
    // MARK: Section - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        completeView()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    // MARK: Section - Protocol Login View
    /* Public Methods */
    func showProfile(user: User) {
        usernameLabel.text = user.fullName!
        profileImage.kf.setImage(with: user.profilePicture)
        postLabel.text = String(describing: user.counts!.media!)
        followersLabel.text = String(describing: user.counts!.followedBy!)
        followingLabel.text = String(describing: user.counts!.follows!)
    }

    // MARK: Section - Public Methods

    @IBAction func logout(_ sender: Any) {
        UserDefaults.standard.removePersistentDomain(forName: "favorties")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.removePersistentDomain(forName: "user")
        UserDefaults.standard.synchronize()
        wireframe?.presentLoginScreenFrom(viewController: self)
    }
    /* Actions */
    // MARK: Section - Actions

    /* Delegates */
    // MARK: Section - Delegates

    /* Private Methods */
    // MARK: Section - Private Methods
    func completeView() {
        self.navigationItem.title = "Profile"
        self.profileImage?.contentMode = UIViewContentMode.scaleAspectFill
        self.profileImage?.clipsToBounds = true
        self.profileImage?.layer.cornerRadius = (self.profileImage?.frame.size.height)! / 2.0
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "favoritoCard"), style: .plain, target: self, action: #selector(favorties))

    }
    
    @objc func favorties() {
        wireframe?.presentFavortiesScreenFrom(viewController: self)
    }

}
