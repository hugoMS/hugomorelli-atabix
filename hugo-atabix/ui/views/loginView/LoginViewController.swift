//
//  LoginViewController.swift
//  hugo-atabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController: BaseViewController, LoginView {

    /* Outlets */
    // MARK: Section - Outlets

    @IBOutlet weak var loginButton: UIButton!
    /* Public Vars */
    // MARK: Section - Public Vars

    var presenter: LoginActionListener?

    /* Private Vars */
    // MARK: Section - Private Vars

    /* UIViewController */
    // MARK: Section - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.imageEdgeInsets = UIEdgeInsets(top: 3, left: 0, bottom: 3, right: 0)
        loginButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 5)
        loginButton.layer.cornerRadius = 5
        loginButton.layer.borderWidth = 1
        loginButton.layer.borderColor = UIColor.black.cgColor
        presenter?.viewDidLoad()
    }

    /* Protocol Login View */
    // MARK: Section - Protocol Login View
    func goToHome() {
        wireframe?.presentTabsNavigationScreenFrom(viewController: self)
    }

    /* Public Methods */
    // MARK: Section - Public Methods

    /* Actions */
    // MARK: Section - Actions

    @IBAction func loginInstagram(_ sender: Any) {
        presenter?.loginInstagram(nav: self.navigationController!)
    }
    /* Delegates */
    // MARK: Section - Delegates

    /* Private Methods */
    // MARK: Section - Private Methods

}
