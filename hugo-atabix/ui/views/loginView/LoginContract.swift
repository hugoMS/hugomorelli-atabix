//
//  LoginContract.swift
//  hugo-atabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation
import UIKit

protocol LoginView: BaseView {
    func goToHome()
}

protocol LoginActionListener: BaseActionListener {
    
    func loginInstagram(nav: UINavigationController)
}
