//
//  LoginPresenter.swift
//  hugo-atabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//
import UIKit
import Foundation

class LoginPresenter: NSObject, LoginActionListener {

    /* Private Vars */
    // MARK: Section - Private Vars
    private var view: LoginView?
    private var instaIteractor: InstagramIteractor?

    /* Constructor */
    // MARK: Section - Constructor

    init (_ view: LoginView, iteractor: InstagramIteractor) {
        self.view = view
        self.instaIteractor = iteractor

    }

    /* Protocol Splash ActionListener */
    // MARK: Section - Protocol Splash ActionListener

    func viewDidLoad() {

    }
    
    func loginInstagram(nav: UINavigationController) {
        instaIteractor?.login(nav: nav, completion: { (user) in
            if user != nil {
                self.view?.goToHome()
            } 
        })
    }
    /* Delegates */
    // MARK: Section - Delegates

    /* Private Methods */
    // MARK: Section - Private Methods

}
