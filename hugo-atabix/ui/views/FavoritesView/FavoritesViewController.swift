//
//  FavoritesViewController.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 05/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import UIKit

class FavoritesViewController: BaseViewController, FavoritesView {
    
    /* Outlets */
    // MARK: Section - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    /* Public Vars */
    // MARK: Section - Public Vars
    
    var presenter: FavoritesActionListener?
    
    /* Private Vars */
    // MARK: Section - Private Vars
     var arrayFavorites: [Media] = []
    /* UIViewController */
    // MARK: Section - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        completeView()
        presenter?.viewDidLoad()
    }
    
    // MARK: Section - Protocol Login View
    /* Public Methods */
    
    func showFavorites(favorites: [Media]) {
        arrayFavorites = favorites
        tableView.reloadData()
    }
    
    // MARK: Section - Public Methods
    
    /* Actions */
    // MARK: Section - Actions
    
    /* Delegates */
    // MARK: Section - Delegates
    
    /* Private Methods */
    // MARK: Section - Private Methods
    func completeView() {
        self.navigationItem.title = "Favorites"
        tableView.dataSource = self
        tableView.delegate = self
               
    }
    
}

extension FavoritesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500.0
    }
}

// MARK: - UITableViewDataSource
extension FavoritesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayFavorites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimeLineCell",
                                                 for: indexPath) as! HomeTableViewCell
        cell.setInfoCell(media: arrayFavorites[indexPath.row], user: arrayFavorites[indexPath.row].user!)
        return cell
    }
}
