//
//  FavoritesContract.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 05/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation

protocol FavoritesView: BaseView {
    
    func showFavorites(favorites: [Media])
    
}

protocol FavoritesActionListener: BaseActionListener {
    
}
