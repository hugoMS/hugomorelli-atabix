//
//  FavoritesPresenter.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 05/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation

class FavoritesPresenter: NSObject, FavoritesActionListener {
    
    /* Private Vars */
    // MARK: Section - Private Vars
    private var view: FavoritesView?
    private var instaIteractor: InstagramIteractor?
    
    /* Constructor */
    // MARK: Section - Constructor
    
    init (_ view: FavoritesView, iteractor: InstagramIteractor) {
        self.view = view
        self.instaIteractor = iteractor

    }
    
    /* Protocol Splash ActionListener */
    // MARK: Section - Protocol Splash ActionListener
    
    func viewDidLoad() {
        if  let favorites = instaIteractor?.getFavorties(){
                self.view?.showFavorites(favorites: favorites)
        }
    }
    
    /* Delegates */
    // MARK: Section - Delegates
    
    /* Private Methods */
    // MARK: Section - Private Methods
    
}
