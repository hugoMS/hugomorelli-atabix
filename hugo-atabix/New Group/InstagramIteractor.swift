//
//  InstagramIteractor.swift
//  hugo-atabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation
import SwiftInstagram

class InstagramIteractor {

    let api = Instagram.shared

    func isLogin() -> Bool {
       // api.logout()
        if api.isAuthenticated {
            return true
        } else {
            return false
        }
    }

    func login(nav: UINavigationController, completion: @escaping (User?) -> Void) {
        api.logout()
        api.login(from: nav, withScopes:
            [.likes, .comments, .followerList, .publicContent], success: {
                self.getUserInfo(completion: { (user) in
                    completion(user)
                })

        }, failure: { error in
                print(error.localizedDescription)
        })

    }

    func getUserInfo(completion: @escaping (User?) -> Void) {
        api.user("self", success: { (user) in
            let user = User(form: user)
            SingletonManager.sharedInstance.setUser(value: user)
            completion(user)
        }) { (error) in
            print(error.localizedDescription)
        }
    }

    func getTimeLine(completion: @escaping ([Media]?) -> Void) {
        let user = SingletonManager.sharedInstance.getUser()
        api.recentMedia(fromUser: (user?.id)!, success: { (medias) in
            var mediaList: [Media] = []
            print(medias)
            for media in medias {
                let media = Media(from: media)
                mediaList.append(media!)
            }
            print(mediaList)
            completion(mediaList)
        }) { (error) in
            print(error.localizedDescription)
        }
    }

    func likePost(media: Media) {
        SingletonManager.sharedInstance.setFavorites(value: media)
    }

    func getUser(idUser: String, completion: @escaping (User?) -> Void) {
        api.user(idUser, success: { (user) in
            let user = User(form: user)
            completion(user)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func getFavorties() -> [Media]? {
        return SingletonManager.sharedInstance.getFavorites()
    }
}
