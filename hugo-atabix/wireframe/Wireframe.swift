//
//  Wireframe.swift
//  hugo-atabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import UIKit

open class Wireframe: Instantiator {

    fileprivate var window: UIWindow?

    // App Navigation
    // MARK: App Navigation

    func configureAppNavigationWithWindow() {
        let iteractor = InstagramIteractor()
        let splashVC = SplashViewController(nibName: nil, bundle: nil)
        splashVC.wireframe = self
        splashVC.presenter = SplashPresenter(splashVC, iteractor: iteractor)

        let splashNavVC = UINavigationController(rootViewController: splashVC)

        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window!.rootViewController = splashNavVC
        self.window!.makeKeyAndVisible()

    }

    func presentLoginScreenFrom(viewController: UIViewController) {
        let iteractor = InstagramIteractor()
        let loginVC = Wireframe.getVCFromStoryBoard(LoginViewController.self,
                                                    storyBoardName: Wireframe.storyLogin) as! LoginViewController
        loginVC.wireframe = self
        loginVC.presenter = LoginPresenter(loginVC, iteractor: iteractor)
        let loginNavVC = UINavigationController(rootViewController: loginVC)
        self.window!.rootViewController = loginNavVC
        self.window!.makeKeyAndVisible()
    }
    
    func presentProfileScreenFrom(viewController: UIViewController, user: User) {
        let iteractor = InstagramIteractor()
        let profileVC = Wireframe.getViewControllerFromStoryBoard("ProfileViewController",
                                                                  storyBoardName:
            Wireframe.storyHome) as! ProfileViewController
        profileVC.wireframe = self
        profileVC.presenter = ProfilePresenter(profileVC, iteractor: iteractor, user: user)
        viewController.hidesBottomBarWhenPushed = true
        viewController.navigationController?.pushViewController(profileVC, animated: true)
        viewController.hidesBottomBarWhenPushed = false
    }
    
    func presentFavortiesScreenFrom(viewController: UIViewController) {
         let iteractor = InstagramIteractor()
        let favoritesVC = Wireframe.getViewControllerFromStoryBoard("FavoritesViewController",
                                                                  storyBoardName:
            Wireframe.storyHome) as! FavoritesViewController
        favoritesVC.wireframe = self
        favoritesVC.presenter = FavoritesPresenter(favoritesVC, iteractor: iteractor)
        viewController.navigationController?.pushViewController(favoritesVC, animated: true)
        
    }

    func presentTabsNavigationScreenFrom(viewController: UIViewController?) {
        let iteractor = InstagramIteractor()
        // homeVC
        let homeVC = Wireframe.getViewControllerFromStoryBoard("HomeViewController",
                                                               storyBoardName:
                                                                   Wireframe.storyHome) as! HomeViewController
        homeVC.wireframe = self
        homeVC.presenter = HomePresenter(homeVC, iteractor: iteractor)

        let homeNavVC = UINavigationController(rootViewController: homeVC)

        // favorites

        // Profile
        let profileVC = Wireframe.getViewControllerFromStoryBoard("ProfileViewController",
                                                               storyBoardName:
            Wireframe.storyHome) as! ProfileViewController
        profileVC.wireframe = self
        profileVC.presenter = ProfilePresenter(profileVC, iteractor: iteractor,
                                               user: SingletonManager.sharedInstance.getUser()!)
        
        let profileNavVC = UINavigationController(rootViewController: profileVC)
        
        // TABS
        let tabsVC = Wireframe.getViewControllerFromStoryBoard("TabsViewController",
                                                               storyBoardName:
                                                                   Wireframe.storyMain) as! UITabBarController
        tabsVC.viewControllers = [homeNavVC, profileNavVC]
        viewController?.navigationController!.present(tabsVC, animated: true, completion: nil)
    }

}
