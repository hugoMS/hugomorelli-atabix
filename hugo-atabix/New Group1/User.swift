//
//  User.swift
//  hugo-atabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation
import SwiftInstagram

class User: NSObject, NSCoding {

    var id: String?
    var username: String?
    var profilePicture: URL?
    var fullName: String?
    var counts: Counts?

    init(form user: InstagramUser) {
        self.id = user.id
        self.username = user.id
        self.profilePicture = user.profilePicture
        self.fullName = user.fullName
        self.counts = user.counts != nil ? Counts(from: user.counts) : Counts.init(media: 0, follows: 0, followedBy: 0)
        
    }

    init(id: String?, username: String?, profilePicture: URL?, fullName: String?, counts: Counts?) {
        self.id = id
        self.username = id
        self.profilePicture = profilePicture
        self.fullName = fullName
        if let counts = counts {
            self.counts = Counts.init(media: counts.media, follows: counts.follows, followedBy: counts.followedBy)
        } else {
            self.counts = Counts.init(media: 0, follows: 0, followedBy: 0)
        }
    }

    required convenience init?(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as? String
        let username = aDecoder.decodeObject(forKey: "username") as? String
        let profilePicture = aDecoder.decodeObject(forKey: "profilePicture") as? URL
        let fullName = aDecoder.decodeObject(forKey: "fullName") as? String
        let counts = aDecoder.decodeObject(forKey: "counts") as? Counts

        self.init(id: id, username: username, profilePicture: profilePicture, fullName: fullName, counts: counts)
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.username, forKey: "username")
        aCoder.encode(self.profilePicture, forKey: "profilePicture")
        aCoder.encode(self.fullName, forKey: "fullName")
        if let count = self.counts {
            aCoder.encode(count, forKey: "counts")
        }

    }

}
