//
//  Counts.swift
//  hugo-atabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation
import SwiftInstagram

class Counts: NSObject, NSCoding {
    var media: Int?
    var follows: Int?
    var followedBy: Int?
    
    init?(from count: InstagramUser.Counts?) {
        guard let count = count else { return nil }
        self.media = count.media
        self.follows = count.follows
        self.followedBy = count.followedBy
    }
    
    init(media: Int!, follows: Int!, followedBy: Int?) {
        self.media = media
        self.follows = follows
        self.followedBy = followedBy
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let media = aDecoder.decodeObject(forKey: "media") as? Int
        let follows = aDecoder.decodeObject(forKey: "follows") as? Int
        let followedBy = aDecoder.decodeObject(forKey: "followedBy") as? Int
        
        self.init(media: media, follows: follows, followedBy: followedBy)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.media, forKey: "media")
        aCoder.encode(self.follows, forKey: "follows")
        aCoder.encode(self.followedBy, forKey: "followedBy")
    }
}
