//
//  Image.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation
import SwiftInstagram

class Image: NSObject, NSCoding {
    var standardResolution: Resolution?
   
    init?(from image: InstagramMedia.Images?) {
        guard let image = image else { return nil }
        self.standardResolution = Resolution(from: image.standardResolution)
        
    }
    
    init(standardResolution: Resolution?) {
        self.standardResolution = standardResolution
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let standardResolution = aDecoder.decodeObject(forKey: "standardResolution") as? Resolution
        
        self.init(standardResolution: standardResolution)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.standardResolution, forKey: "standardResolution")
    }
    
}
