//
//  Resolution.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation
import SwiftInstagram

class Resolution: NSObject, NSCoding {
    
    var url: URL?

    init?(from resolution: InstagramMedia.Resolution? ) {
        guard let resolution = resolution else { return nil }
        self.url = resolution.url
    }
    
    init(url: URL?) {
        self.url = url
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let url = aDecoder.decodeObject(forKey: "url") as? URL
        self.init(url: url)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.url, forKey: "url")
    }
}
