//
//  Media.swift
//  hugoAtabix
//
//  Created by Hugo Morelli on 04/11/2017.
//  Copyright © 2017 Hugo Morelli. All rights reserved.
//

import Foundation
import SwiftInstagram

class Media: NSObject, NSCoding {
    
    var id: String?
    var user: User?
    var createdDate: Date?
    var type: String?
    var images: Image?
    
    init?(from media: InstagramMedia?) {
        guard let media = media else { return nil }
        self.id = media.id
        self.user = User(form: media.user)
        self.createdDate = media.createdDate
        self.type = media.type
        self.images = Image(from: media.images)

    }
    
    init(id: String?, user: User?, createdDate: Date?, type: String?, images: Image? ) {
        self.id = id
        self.user = user
        self.createdDate = createdDate
        self.type = type
        self.images = images
       
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as? String
        let user = aDecoder.decodeObject(forKey: "user") as? User
        let createdDate = aDecoder.decodeObject(forKey: "createdDate") as? Date
        let type = aDecoder.decodeObject(forKey: "type") as? String
        let images = aDecoder.decodeObject(forKey: "images") as? Image
            
        self.init(id: id, user: user, createdDate: createdDate, type: type, images: images)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.user, forKey: "user")
        aCoder.encode(self.createdDate, forKey: "createdDate")
        aCoder.encode(self.type, forKey: "type")
        aCoder.encode(self.images, forKey: "images")
    }
    
}
