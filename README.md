#Test Atabix


I decided to use Swift 4

## Project Architecture
* [Clean Swif](http://bemobile.es/blog/2016/11/clean-architecture-the-bemobile-way/)

## Design pattern
* Model-View- Presenter (MVP)



## Development tools

I used the following development tools:

* Xcode 9
* Cocoapods 1.3.1

## Programming languages

* Swift 4

## External libraries

* Kingfisher -> handling image
* SwiftInstagram -> wrapper to connecto to ig
* SwiftLint -> to clean code and remove error or warning
* ReachabilitySwift -> to valiate connection


## References

* [Model-View-Presenter design pattern](https://es.wikipedia.org/wiki/Modelo%E2%80%93vista%E2%80%93presentador)

